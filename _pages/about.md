---
permalink: /about/
title: "Tentang"
---
## Apa ini?

**Seluk Beluk** adalah sebuah direktori aneh berupa **cuplikan - cuplikan artikel** atau **apa saja** yang _nge-hits_ menurut saya. Bukan bermaksud menjiplak, tetapi mengambil beberapa paragraf pentingnya saja (atau bisa juga _seluruh artikel_ alias _copas_) sembari **menyematkan tautan menuju artikel asli**.

## Buat apa coba?

Semacam pembatas buku. Mengingatkan saya di masa depan bahwa saya di masa lampau pernah tertarik dengan artikel yang dicuplik secara paksa. Siapa tahu **Seluk Beluk** berguna sebagai pengingat dan rujukan **untuk saya di masa depan**.

## Macam apa?

Tergantung. Bisa berupa artikel berita, ulasan atau video (di tandai _video_{:.fa fa-video}), podcast (ditandai dengan _podcast_{:.fa fa-podcast}, atau gabungan dari kesemuanya itu.

## Hak cipta

Untuk pemilik konten yang keberatan karyanya di _comot_ sebagian di **Seluk Beluk**, silahkan kirimkan komplain melalui profil media sosial tersemat. Japri mesra ya..
