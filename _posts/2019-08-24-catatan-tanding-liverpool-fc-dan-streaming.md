---
title: "Catatan Pertandingan Liverpool FC dan Link Streaming [Update]"
excerpt: "Manchester United vs Liverpool FC, Sabtu 20/10/2019 Kick-Off 22:30 WIB. Premier League Game Week 8"
header:
 og_image: "https://i2-prod.liverpool.com/incoming/article17039930.ece/ALTERNATES/s810/0_GettyImages-1179188179-1.jpg"
 video:
  id: c8B9P7WAsbA
  provider: youtube
categories: [streaming]
tags: [streaming bola, streaming liverpool, rekap liverpool fc]
last_modified_at: 2019-10-06 11:11:21
redirect_from:
 - /streaming/streaming-m3u8/
 - /liverpool
 - /streaming/m3u8/
---
{% include figure alt="Liverpool Supporters!" caption="Anfield's mentality has been transformed." image_path="https://i2-prod.liverpool.com/incoming/article17041156.ece/ALTERNATES/s810/0_GettyImages-1173868960.jpg" %}

*Ingat baik-baik!*{:.fa .fa-cog .fa-spin aria-hidden="true"} **1**. Tayangan akan tersedia hanya **jika ada** dan **tersedia** (dapat saya temukan) di internet. Baca [latar belakang](#latar-belakang) catatan ini. **2**. Refresh **10 menit** sebelum _kick off_, selain di waktu tersebut kemungkinan siaran tidak dapat diakses. **3**. Streaming di halaman ini diusahakan dalam kualitas HD, jadi kemungkinan akan menguras kuota/data hingga 2 GB dan membutuhkan kecepatan konektivitas minimal 10 mbps untuk kelancaran streaming. **4**. Sedangkan mengenai pilihan siaran **MolaTV**, tidak saya rekomendasikan. Silahkan baca [catatannya](#molatv).
{:.notice--warning}

## Catatan pertandingan dan streaming

{% include figure caption="Premiere League Standing Minggu ke-8" image_path="https://i1.wp.com/knoacc.github.io/catetan/assets/images/Screenshot_20191006-235423.jpg?w=460" url="/assets/images/Screenshot_20191006-235423.jpg" alt="EPL 2019/2020 Standing"%}

Postingan ini niatnya hanya untuk menyimpan streaming pertandingan-pertandingan **Liverpool FC** saja. Utamanya *Premier League*, tetapi mungkin ada juga pertandingan di kompetisi lain seperti _Carabao Cup_ dan _Champions League_.

<blockquote>Waktu lokal sekarang: <strong><span id="JamLokal" onload="showTime()"></span></strong></blockquote>
<script>
function showTime(){
    var date = new Date();
    var h = date.getHours();
    var m = date.getMinutes();
    var s = date.getSeconds();
    var time = h + ":" + m + ":" + s;
    document.getElementById("JamLokal").innerText = time;
    document.getElementById("JamLokal").textContent = time;
    setTimeout(showTime, 1000);
}
showTime();
</script>

### Jadwal Selanjutnya:

**EPL GW 9** Minggu (20/10/2019) **Manchester Utd. vs Liverpool FC** [A]. **KO 22:30 WIB**.

Last season away match (2018/2019) highlight:

{% include video id="-Q5bKEEQRps" provider="youtube" %}

### Hasil Sebelumnya:

**EPL GW 8** Minggu (05/10/2019) **Liverpool FC vs Leicester City** [H]. **KO 21:00 WIB**.

{% include figure alt="sadio mane diving" image_path="https://i2-prod.liverpool.com/incoming/article17039937.ece/ALTERNATES/s810/0_GettyImages-1179213601.jpg" caption="Sadio Mane diving? Oh Yeah, 🙄 definitely not enough contact to award a penalty. Fuck that!" %}

- Artikel pra-tanding dari portal [This Is Anfield](https://www.thisisanfield.com/2019/09/brendan-rodgers-excited-for-first-return-to-incredible-former-club-liverpool/). Baca saja sambil tiduran.
- Gol! **S. Mane** 40'. Assist dari **J. Milner**.
- Babak ke-2 berjalan intens, Liverpol banyak peluang, banyak shoot on target, tak ada goal.
- Gol balasan datang 10 menit menjelang akhir waktu normal. **J. Maddison** assist dari **A. Perez**
- **Mo. Salah** [cidera](/assets/images/Screenshot_20191007-125401.jpg) dan di ganti **Adam Lallana** saat _injury time_, **S. Mane** di langgar di menit 90+3'. Penalty! Gol,**J. Milner** 90+5'. Catatan soal _S. Mane yang dituduh diving_ via [liverpool.com](https://www.liverpool.com/liverpool-fc-news/features/shearer-mane-liverpool-leicester-penalty-17039969) 😂
- Ribut-ribut pemain vs **A. Perez** di akhir pertandingan 🙄 [oh dikomandani oleh Adam Lallana](https://i2-prod.liverpool.com/incoming/article17040575.ece/ALTERNATES/s810/0_GettyImages-1173858117.jpg){:title="At full-time, Andy Robertson gave Ayoze Perez a slight elbow to the ribs on his way to celebrate with Lallana and Virgil van Dijk, only for the Spaniard to retaliate with a heavy shove in the Scotsman's back."}.
- Review Liverpool FC vs Leicester City via [This Is Anfield](https://www.thisisanfield.com/2019/10/liverpool-2-1-leicester-player-ratings-what-the-media-and-statistics-say/)

**Champions League** Group Stage Matchday 2: _Liverpool FC_ vs _Red Bull Salzburg_ [H]. **KO 02:00 WIB**, Kamis, 3 Oktober 2019. Skor akhir: **4-3**
{: .notice .notice--info}

{% include figure caption="Gol penyelamat dari Mo Salah" alt="Mo Salah vs RB Salzburg" image_path="https://i2-prod.liverpool.com/incoming/article17022711.ece/ALTERNATES/s810/0_GettyImages-1178635744.jpg" %}

Lihat jadwal matchday 2 selengkapnya [di sini](/sepakbola/jadwal-liga-champions-matchday-2-group/).

- Artikel pra-tanding dari portal [This Is Anfield](https://www.thisisanfield.com/2019/09/key-men-manager-impressions-a-curious-link-to-the-past-complete-lowdown-on-salzburg/), [Liverpool.com](https://www.liverpool.com/liverpool-fc-news/features/liverpool-salzburg-man-united-haaland-17001599) dan [Liverpool Echo](https://www.liverpoolecho.co.uk/sport/football/football-news/former-liverpool-man-identifies-key-17007724) bisa dibaca sambil ngopi.
- Mungkin akan ditayangkan TV Lokal (SCTV).
- 3 Gol dari **S. Mane** 9', **A. Robertson** 25', **Mo Salah** 36' di babak pertama.
- Gol balasan Redbull oleh **H. Hwang** 39'.
- Babak kedua Liverpool bermain dibawah ekspektasi. 2 Gol balasan dari **M. Takumi** 56' dan **E.B. Halland** 60' hanya berselang kurang dari 5 menit.
- Untungnya gol **Mo Salah** 69' mampu membuat Liverpool memimpin kembali.
- Babak kedua berlangsung sangat terbuka. Banyak kesalahan yang dibuat pemain Liverpool; salah umpan, salah antisipasi.
- Si Mini Me _mantep uga_ mainnya. “Liverpool have reportedly identified Red Bull Salzburg winger **Takumi Minamino** as a potential signing.”

**EPL GW 7** 28 September 2019 **Sheffield Utd. vs Liverpool FC** [A]. **KO 18:30 WIB**. Skor akhir 0-1 untuk tim tandang. Liverpool adem ayem di _putjuk_.

[![Wijnaldum](https://www.liverpoolecho.co.uk/incoming/article16998248.ece/ALTERNATES/s510b/0_JS194232247.jpg G. Wijnaldum)](https://www.liverpoolecho.co.uk/incoming/article16998248.ece/ALTERNATES/s810/0_JS194232247.jpg){:title="Georginio Wijnaldum’s goal, courtesy of Dean Henderson’s blunder in the Blades net, was the difference."}

- Tidak tayang di TV Lokal (TVRI) maupun MolaTV, tetapi tayangan ulang bisa dilihat di [Web/App](https://mola.tv/watch?v=vd68968790){: onclick="window.open(this.href, 'window', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" title="Tonton Tayang Ulang" target="_new" rel="nofollow noreferer noopener"}
- Gol di babak kedua dari tendangan keras **G. Wijnaldum** 70' yang tak mampu ditangkap dengan baik dan lolos dari kolong kaki penjaga gawang.
- Jadwal streaming/hasil pertandingan pekan ke-7 bisa dilihat [di sini](https://live.istimiwir.host/epl-7)

**Football League Cup** 3<sup>rd</sup> round: _MK Dons_ vs _Liverpool FC_ [A]. Kamis, 26 September 2019 **KO 01:45 WIB**. Skor akhir 0-2 untuk kemenangan Liverpool FC. _**note:** lebih dikenal sebagai **Carabao Cup**_

- Tidak tayang di TV Lokal manapun, tetapi masih bisa disaksikan melalui MolaTV [Web/App](https://mola.tv/watch?v=vd68970017){: onclick="window.open(this.href, 'window', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" title="Tonton Tayang Ulang" target="_new" rel="nofollow noreferer noopener"}, lihat [catatan](#molatv).
- Gol **J. Milner** 41' di babak pertama, dan **Ki-jana Hoever** 69' di babak kedua.
- **C. Jones** lagi apes, dua tembakan terarah ke gawang cuma membentur mistar.

**EPL GW 6** 22 September 2019 **Chelsea vs Liverpool FC** [A] KO 22:30. Skor akhir 1-2, LFC [tetap di _putcjuk_](#standing) meskipun di laga lain **Man. City** memasukkan 8 gol tanpa balas ke gawang **Watford**

![Firmino ft. Fabinho](https://www.thisisanfield.com/wp-content/uploads/P2019-09-22-039-Chelsea_Liverpool-1-e1569168573519.jpg){:title="Firmino ft. Fabinho"}

- Tayangan langsung tidak tersedia, baik TVRI maupun MolaTV [Web/App](https://mola.tv/watch?v=vd68620074){: onclick="window.open(this.href, 'window', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" title="Tonton Tayang Ulang" target="_new" rel="nofollow noreferer noopener"}, tetapi bisa ditonton tayangan ulangnya.
- Gol dari skema set piece: **TAA** 14' dan sundulan **Firmino** 30', kedudukan 2-0 di babak pertama.
- Babak 2 **Kante** 71' melakukan akselerasi dan tendangan kesisi kiri gawang **Adrian** dan tak dapat dijangkau. 
- Liverpool bertahan dengan bombardir serangan dari pemain-pemain Chelsea, kedudukan 2-1 tak berubah hingga akhir pertandingan.

**Champions League** Group Stage Matchday 1: _SC Napoli_ vs _Liverpool FC_ [away]. **KO 02:00 WIB**, Rabu, 17 September 2019. **Hasil: Kalah 0-2**.
{: .notice .notice--info}

[![Adrian vs Napoli](https://i2-prod.liverpool.com/incoming/article16936166.ece/ALTERNATES/s510b/0_GettyImages-1168930358.jpg Adrian vs Napoli)](https://i2-prod.liverpool.com/incoming/article16936166.ece/ALTERNATES/s810/0_GettyImages-1168930358.jpg){:title="Napoli mengukuhkan kemenangan kandang dengan gol kedua di menit-menit akhir"}

Lihat hasil matchday 1 selengkapnya [di sini](/sepakbola/jadwal-liga-champions-matchday-1-group/)

**EPL GW 5** 14 September 2019 **Liverpool vs Newcastle** [A] KO 18:30 WIB. Skor akhir 3-1, Liverpool FC tetap di pucuk.

- Tidak ditayangkan LIVE di TVRI maupun MolaTV [Web/App](https://mola.tv/watch?v=vd67493643){: onclick="window.open(this.href, 'window', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" title="Tonton Tayang Ulang" target="_new" rel="nofollow noreferer noopener"}, tetapi bisa ditonton tayangan ulangnya.
- Link stream `m3u8` <del>hari ini masih burem 😒 gelap gulita. Jadi di ganti tayangan channel eurosport.</del>
- **[FIX]** Sudah bisa. Dapat stream dari [NBCSN](https://en.wikipedia.org/wiki/NBCSN) 😌 kalau mau, lanjut nonton TOT vs CRY KO 21:00.
- Kebobolan, Gol NEW: **J. Willems** 7'. 0-1
- Gol balasan: **S. Mane** 28', assist dari **Robertson**. 1-1
- Gol lagi, **S. Mane** 40', dari bola muntah rebutan **Firmino** dengan **Atsu**.
- Gol **Mo Salah** 72', assist keren dari **Firmino**. 3-1 untuk Liverpool FC.

**EPL GW 4** 31 Agustus 2019 **Burnley vs Liverpool** [A] KO 23:30 WIB. Skor akhir 0-3 untuk kemenangan Liverpool.

- Tidak ditayangkan LIVE di MolaTV [Web/App](https://mola.tv/watch?v=vd66534925){: onclick="window.open(this.href, 'window', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" title="Tonton Tayang Ulang" target="_new" rel="nofollow noreferer noopener"}, tetapi bisa ditonton tayangan ulangnya.
- Goal LFC: **C. Wood** 33' (og), S. Mane 37', R. Firmino 80'.
- _Own goal_ C. Wood _deflected_ dari tendangan keras **T.A. Arnold** umpan **J. Henderson**.

> [Menerawang Starting XI Liverpool 5 Tahun Mendatang](https://catetan.istimiwir.host/sepakbola/liverpool-5-tahun-mendatang-tanpa-salah-mane-dan-van-dijk/){: rel="noopener"}

**EPL GW 3** 24 Agustus 2019 **Liverpool vs Arsenal** [H] KO 23:30 WIB. Skor akhir 3-1 untuk kemenangan Liverpool

- Tayang di MolaTV [Web/App](https://mola.tv/watch?v=vd65950609){: onclick="window.open(this.href, 'window', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" title="Tonton Tayang Ulang" target="_new" rel="nofollow noreferer noopener"}
- Goal LFC: **Joel Matip** 41', **M. Salah** 49' (p), 58'. 
- Goal ARS: **L.T. Di Pascua** 85'.

## Tambahan-tambahan

{% include figure image_path="https://s.france24.com/media/display/9b3e331e-84b5-11e9-9042-005056a964fe/w:1024/p:16x9/football-liverpool-ligue-champions-reds-tottenham.jpg" caption="Juara Champions League 2018/2019, Gelar ke-6 Liverpool FC" alt="Champions League 2018/2019 Winner" %}

**Latar belakang catatan ini**{:#latar-belakang}

Latar belakang sebelum memutuskan untuk mencatat setiap laga Liverpool FC (setelah GW-2) di halaman ini:

Kalau di-ingat-ingat sudah kenal/tahu **Liverpool FC** sejak tanpa sengaja membeli stiker logonya (sticker bahan gravtech) di pasar desa saat masih SD/MI karena desain logonya menarik. Tetapi baru benar-benah _ngeh_ dan fanatik setelah berita **Epic Comeback** Liverpool FC melawan AC Milan di Istambul di _blow-up_ beberapa majalah bola. Ketika itu baru saja lulus kuliah (D1) dan sibuk pacaran, malas kerja. Jujur saja, baru benar-benar intens menonton setiap pertandingan Liverpool FC sejak nyaris juara di EPL 2013/2014, jamannya Suarez dkk. dilatih Ojes dan Gerrard kepleset. Sejak itu pula selalu berburu link-link streaming setiap pertandingan Liverpool FC ketika tak ada **streaming tayangan resmi yang gratis**. Utamanya ketika tidak ditayangkan di TV lokal atau tak ada tempat untuk _nebeng_ tontonan TV kabel hingga sekarang. Apalagi, kini semakin sulit karena hak siar berpindah dari tangan FMA 😭.

**Mengenai MolaTV dan Tips Untuk Nonton Streaming via MolaTV Web**{:#molatv}

Untuk wilayah Indonesia bisa melihat di laman web [mola.tv](https://mola.tv/){: rel="nofollow noreferrer noopener" target="_blank"} sebagai pemegang hak siar resmi di Indonesia.
Agar bisa melihat dengan browser Chrome Mobile (Android) tanpa harus memasang aplikasi, aktifkan/centrang pilihan Situs dekstop dari menu Chrome.

**Pendapat pribadi:**
- Aplikasi mola.tv **sangat tidak saya rekomendasikan**, sering _force closed_ tanpa alasan. Aplikasi sampah, selalu meminta pembaruan, mungkin karena masih beta.
- Untuk yang gratisan, kualitas tayangan terbatas hingga 569p saja dan ada tampilan iklan yang berakibat tidak bisa menikmati tampilan layar penuh. Sedangkan yang berbayar, untuk pertandingan tertentu juga kadang tak tersedi di Web maupun Aplikasinya.
- Kalaupun masih mau yang berbayar, mending sekalian saja langganan Matrix. Percuma bayar langganan premium jika cuma menonton via aplikasi atau web streaming.
